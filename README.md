# ** POC Projeto 8 - Pesquisa OD de Cargas**  #


O sistema apresentado nesta POC é um exemplo de uso de um componente de filtragem em busca de dados. Foi programado em Java EE, utilizando a versão 8.

## Uso do Componente de Filtragem (Frontend) ##

O componente de filtragem foi feito na versão 1.3.15 do framework AngularJS. Ele contempla filtros para os seguintes tipos de dados e com as seguintes restrições:

### Texto ###

Dados do tipo texto podem ser filtrados por meio das operações **Contém**, **Começa com** e **Termina com**.

### Numérico ###

Dados do tipo numérico podem ser filtrados por meio das operações **Menor que**, **Igual a**, **Diferente de** e **Maior que**.

### Enumeração ###

Dados do tipo enumeração (ou categoria) podem ser filtrados por meio da operação **Igual a**.

### Data ###

Dados do tipo data podem ser filtrados por meio das operações **Anterior a**, **Exatamente em** e **Posterior a**.

A estrutura do componente de filtragem é composta por:

* Duas partials em HTML;

* Uma diretiva;

* Dois controllers;

* Uma service.

Uma das partials contém o modal para escolha do campo a ser filtrado e do valor do filtro. A outra exibe os filtros ativos. Cada uma das partials tem seu controller correspondente.

O controller do modal monta o objeto do filtro e o envia para o controller da listagem de filtros ativos que, por sua vez, adiciona o filtro na tela e o envia para a service de filtragem. Após a adição do filtro na service, a página que utiliza o componente realizará uma chamada ao servidor para que os registros sejam recarregados e o conteúdo da tabela seja filtrado.

Para que o filtro seja interpretado pelo backend, todas as restrições são enviadas pelos queryParameters da chamada REST e seguem o padrão esperado pela interface. 

Foram estabelecidas as seguintes regras para o mapa de restrições **textuais**:

* Sufixo *__like* após o nome(ou caminho) da propriedade do modelo, para restrição do tipo "Contém";

* Sufixo *__startsWith* após o nome(ou caminho) da propriedade do modelo, para restrição do tipo "Começa com";

* Sufixo *__endsWith* após o nome(ou caminho) da propriedade do modelo, para restrição do tipo "Termina com".

Foram estabelecidas as seguintes regras para o mapa de restrições **numéricas**:

* Sufixo *__lte* após o nome(ou caminho) da propriedade do modelo, para restrição do tipo "Menor que";

* Sufixo *__gte* após o nome(ou caminho) da propriedade do modelo, para restrição do tipo "Maior que";

* Sufixo *__isnot* após o nome(ou caminho) da propriedade do modelo, para restrição do tipo "Diferente de".

Foram estabelecidas as seguintes regras para o mapa de restrições de **data**:

* Sufixo *__lte* após o nome(ou caminho) da propriedade do modelo, para restrição do tipo "Menor que";

* Sufixo *__gte* após o nome(ou caminho) da propriedade do modelo, para restrição do tipo "Maior que";

Para dados do tipo enumeração (ou categoria), nenhum sufixo precisa ser enviado.

Para comparações do tipo "igual a", tanto para **datas** quanto para **números**, a composição das seguintes restrições é usada:

* Menor que o último valor anterior ao valor de restrição;

* Maior que o primeiro valor posterior ao valor de restrição.

Para utilizar o componente, é necessário que o controller da página que exibirá as informações injete a service de filtragem e passe as seguintes propriedades:

* Função que realiza a chamada para o carregamento das informações;

* Lista de campos que possam ser restringidos pela filtragem.

## Uso do Componente de Filtragem (Backend) ##

As etapas descritas abaixo, referentes ao backend, podem ser realizadas para que um desenvolvedor possa utilizar o componente de filtragem desenvolvido em um modelo de dados próprio que deva ser filtrado.

### Persistência ###

Nesta etapa, devem ser criados um modelo que representará uma tabela no banco e um DAO referente a ele e que deverá estender a classe BaseDAO.

BaseDAO é a classe base para todos os Data Access Objects (DAOs) do sistema. Ela contém métodos genéricos para operações CRUD. O método fundamental utilizado neste projeto é o 

```
#!java

public Page<T> paginatedList(final Map<String, List<String>> parameters, final Short pageNumber, final String... leftJoins)
```
.

Este método retorna uma Page do modelo T em questão, dado um mapa de parâmetros de filtragem, o número da página desejada e, opcionalmente, uma lista de leftJoins.

Uma Page é um wrapper para uma lista de objetos, o número da página e a quantidade de objetos nela contidos.

### Serviço ###

Nesta etapa deve ser criado um serviço que implemente a interface BaseServiceInterface, com o modelo de dados criado acima como parâmetro. BaseServiceInterface é a interface que contém métodos default para operações CRUD. Classes que implementem a interface BaseServiceInterface têm que implementar o método 


```
#!java

package BaseDAO<T> getDAO()
```


, que retorna o Data Access Object de um modelo.
  
### REST ###

Nesta etapa deve ser criado um endpoint que estenderá o BaseResource e terá o serviço (service) criado no item acima injetado. BaseResource é a classe base para todas as classes de Endpoints do sistema. Contém as implementações padrão de métodos HTTP GET, POST, PUT e DELETE. O método utilizado neste projeto foi o


```
#!java

@GET
@Produces(MediaType.APPLICATION_JSON)
public Response listResources()
```

Para este método, dada uma lista de queryParameters, uma Response é gerada contendo um objeto da classe Container. Este objeto funciona como wrapper para uma coleção de objetos de modelo e um atributo de metadados; é no objeto de metadados que está contida a lista de campos a serem exibidos na tela.

Para a utilização deste método, é necessário que a classe filha implemente os métodos obtainResources e generateCamposListagem. 

### Sequência de Operações de uma Chamada ###

Ao receber uma requisição de listagem de dados, o método 


```
#!java

public Response listResources()
```


do BaseResource será ativado. Ele construirá uma HTTP Response 200 com o objeto da classe Container. Para que isto seja possível, a classe que herda de BaseResource fornecerá os campos de listagem e os dados de modelo.

Para obter os dados de modelo, o serviço referente ao modelo será acionado. Quando o serviço chamar o DAO para realizar a busca, o BaseDAO terá seu método 


```
#!java

public Page<T> paginatedList(final Map<String, List<String>> parameters, final Short pageNumber, final String... leftJoins) 
```


ativado. A construção da Page irá levar em consideração todos os filtros enviados no queryParameters. Para cada restrição, é analisado qual o sufixo utilizado para que se monte a query do JPA com a comparação correta. O nome (ou caminho) da propriedade do objeto a ser buscado é utilizado para se definir o tipo de dado a ser filtrado. Isto gerará uma lista de predicados a serem utilizados na query. 
 
# Funcionamento do Sistema #

Esta seção descreve a maneira como o componente de filtragem deve ser operado.

Inicialmente, o usuário visualiza a listagem de dados de interesse e pode decidir filtrá-la de acordo com os valores de um ou mais de seus campos. O componente de filtragem pode ser acessado por meio do botão "Filtrar", conforme ilustrado na Figura 1.

###
![11.png](https://bitbucket.org/repo/76AeM8/images/290944801-11.png)
###

**Figura 1 - Tabela de dados com botão de filtragem**

Após, o usuário seleciona o campo a partir do qual deseja filtrar os dados. Para o objeto de exemplo aqui apresentado (isto é, para o objeto de Cargas), os seguintes campos podem ser selecionados: Categoria, Peso, Volume, CEP de destino, Data de saída e Data de chegada (vide Figura 2).

###
![22.png](https://bitbucket.org/repo/76AeM8/images/418451560-22.png)
###

**Figura 2 - Campos para filtragem para objeto do tipo Carga**

Após selecionar o campo desejado, o usuário deverá selecionar o tipo de filtro (variável de acordo com o tipo do campo) e o valor pelo qual deseja filtrar (vide Figura 3).

###
![33.png](https://bitbucket.org/repo/76AeM8/images/3922741565-33.png)
###

**Figura 3 - Escolha do tipo e do valor do filtro de acordo com o tipo do campo selecionado **

O usuário então clica no botão “Adicionar” para que o filtro seja concatenado à lista de filtros já existente (se existir). A partir do conjunto de filtros adicionados até o momento, é gerada uma chamada de consulta ao backend. O sistema aguarda o retorno do backend e então exibe os dados filtrados em uma tabela que pode ser visualizada pelo usuário (vide Figura 4)

###
![44.png](https://bitbucket.org/repo/76AeM8/images/3124730339-44.png)
###

**Figura 4 - Dados filtrados **

Este ciclo de uso pode ser repetido de forma a concatenar diferentes filtros, aumentando assim a especificidade da busca de dados.

Para excluir um filtro criado, o usuário pode clicar sobre o botão que o representa (situado ao lado do botão "Filtrar"); isto fará com que a consulta não mais considere aquela filtragem.

# Como configurar #

O projeto possui dependência de um plugin do MAVEN para geração dos metamodels do JPA a partir das classes de modelo. Esta dependência pode ser obtida por meio do link abaixo.

http://mvnrepository.com/artifact/org.bsc.maven/maven-processor-plugin/3.1.0-beta1

Para a utilização do sistema, é necessária a instalação da versão 3.1.0 do MAVEN ou superior. Esta pode ser obtida através do link abaixo.

https://maven.apache.org/download.cgi

O arquivo POM (Project Object Model) do projeto está configurado para a versão 1.8 do JAVA, e pode ser acessado por meio do link abaixo.

http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html


# Configuração do banco #

O banco de dados deve ser criado com nome, owner e senha iguais a "base".

No standalone.xml do JBOSS deve ser adicionada a configuração do banco:


```
#!xml

<datasource jta="true" jndi-name="java:jboss/datasources/baseDS" pool-name="baseDS" enabled="true" use-java-context="true">
    <connection-url>jdbc:postgresql://localhost:5432/base</connection-url>
    <driver>postgresql</driver>
    <security>
        <user-name>base</user-name>
        <password>base</password>
    </security>
</datasource>
```