package br.com.base.business;

import javax.inject.Inject;

import br.com.base.dao.BaseDAO;
import br.com.base.dao.Carga;
import br.com.base.dao.CargaDAO;

/**
 * Serviço de carga.
 */
public class CargaService implements BaseServiceInterface<Carga> {

	@Inject
	CargaDAO cargaDAO;
	
	@Override
	public BaseDAO<Carga> getDAO() {
		return cargaDAO;
	}

}
