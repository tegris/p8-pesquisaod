package br.com.base.business;

import java.util.List;
import java.util.Map;

import br.com.base.dao.BaseDAO;
import br.com.base.dao.Page;
import br.org.dsd.common.jpa.model.LongModel;

/**
 * Classe base para classes de negócio de entidades do banco.
 * @author wilerson.oliveira
 * @param <T> classe da entidade que o serviço deve atender
 */
public interface BaseServiceInterface<T extends LongModel> {

    /**
     * Método interno para obter a implementação do DAO.
     * @return implementação do DAO para a classe
     */
    BaseDAO<T> getDAO();

    /**
     * Método para localizar um objeto pelo ID. A implementação default é um pass-through para o DAO.
     * @param id ID do objeto
     * @return objeto localizado, ou null se não existir
     */
    default T findById(final Long id) {
        return getDAO().findById(id, false);
    }

    /**
     * Método para listar os objetos da classe. A implementação default é um pass-through para o DAO.
     * @return lista de objetos
     */
    default List<T> findAll() {
        return getDAO().findAll();
    }

    /**
     * Método para listar objetos, filtrados por parâmetros.
     * @param parameters mapa contendo os parâmetros da classe que serão filtrados e os possíveis valores que podem estar contidos.
     * @return lista de objetos
     */
    default List<T> list(final Map<String, List<String>> parameters) {
        return getDAO().list(parameters);
    }

    /**
     * Método para listar objetos, filtrados por parâmetros e com paginação.
     * @param parameters mapa contendo os parâmetros da classe que serão filtrados e os possíveis valores que podem estar contidos
     * @param pageNumber número de página que se deseja obter
     * @return objeto de página contendo os objetos da página solicitada e a contagem total de objetos correspondentes à busca
     */
    default Page<T> paginatedList(final Map<String, List<String>> parameters, final Short pageNumber) {
        return getDAO().paginatedList(parameters, pageNumber);
    }

    /**
     * Método para listar objetos, filtrados por parâmetros, com paginação e ordenação.
     * @param parameters mapa contendo os parâmetros da classe que serão filtrados e os possíveis valores que podem estar contidos
     * @param orderBy campo para ordenação.
     * @param ascending indica se a ordenação é ascendente ou descendente.
     * @param pageNumber número de página que se deseja obter
     * @return objeto de página contendo os objetos da página solicitada e a contagem total de objetos correspondentes à busca
     */
    default Page<T> paginatedOrderedList(final Map<String, List<String>> parameters, final String orderBy, final boolean ascending, final Short pageNumber) {
        return getDAO().paginatedOrderedList(parameters, orderBy, ascending, pageNumber);
    }

    /**
     * Método para persistir um objeto no banco. A implementação default é um pass-through para o DAO.
     * @param object objeto a ser persistido
     * @return instância atualizada do objeto
     */
    default T saveOrUpdate(final T object) {
        return getDAO().saveOrUpdate(object);
    }

    /**
     * Método para remover um objeto do banco através do ID. A implementação default é um pass-through para o DAO.
     * @param id identificador do objeto a ser removido
     */
    default void removeById(final Long id) {
        getDAO().removeById(id);
    }

}
