package br.com.base.dao.filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Filtro para contenção em sets de primitivas ou enums.
 * @author wilerson.oliveira
 */
public class InJoinFilter implements Filter {

    private Path path;
    private Object value;

    /**
     * {@inheritDoc}
     */
    @Override
    public Predicate buildPredicate() {
        return path.as(String.class).in(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InJoinFilter setCriteriaBuilder(CriteriaBuilder cb) {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InJoinFilter setPath(Path path) {
        this.path = path;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InJoinFilter setConvertedValue(Object value) {
        this.value = value;
        return this;
    }
}
