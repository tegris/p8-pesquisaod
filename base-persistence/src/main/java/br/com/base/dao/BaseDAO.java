package br.com.base.dao;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.org.dsd.common.jpa.model.LongModel;
import br.org.dsd.common.jpa.repository.AbstractRepository;
import br.org.dsd.common.jpa.repository.RegistroNaoEncontradoException;

/**
 * Classe base de acesso ao banco de dados.
 * @author wilerson.oliveira
 * @param <T> tipo de classe da entidade do banco
 */
public abstract class BaseDAO<T extends LongModel> extends AbstractRepository<T> {

    /**
     * Número de resultados por página
     */
    protected static final int RESULTS_PER_PAGE = 20;

    /**
     * Remove um objeto da base através do ID.
     * @param id identificador do objeto
     */
    public void removeById(final Long id) {
        T resource = findById(id, false);
        if (resource == null) {
            throw new RegistroNaoEncontradoException(getEntityType(), id);
        }
        remove(resource);
    }

    /**
     * Lista objetos procurando por restrições básicas.
     * @param parameters parâmetros para filtrar os objetos
     * @return lista de objetos
     */
    public List<T> list(final Map<String, List<String>> parameters) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(getEntityType());
        Root<T> from = query.from(getEntityType());
        query.where(getPredicates(parameters, cb, from));
        return getEntityManager().createQuery(query).getResultList();
    }

    /**
     * Lista objetos paginados procurando por restrições básicas.
     * @param parameters parâmetros para filtrar os objetos
     * @param pageNumber número de página desejado
     * @return objeto de página contendo a lista de objetos e contagem total dos objetos que correspondem à busca
     */
    public Page<T> paginatedList(final Map<String, List<String>> parameters, final Short pageNumber, final String... leftJoins) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(getEntityType());
        Root<T> from = query.from(getEntityType());
        Predicate predicates = getPredicates(parameters, cb, from, leftJoins);

        query.where(predicates);
        TypedQuery<T> typedQuery = getEntityManager().createQuery(query);
        // Caso o número de página seja -1, traz todos os resultados.
        if (pageNumber != -1) {
            typedQuery.setMaxResults(RESULTS_PER_PAGE).setFirstResult((pageNumber - 1) * RESULTS_PER_PAGE);
        }
        Page<T> page = new Page<>();
        page.setPageNumber(pageNumber);
        page.setObjects(typedQuery.getResultList());
        // Caso o número de página seja -1, traz todos os resultados.
        if (pageNumber != -1) {
            page.setTotalObjects(count(parameters));
        } else {
            page.setTotalObjects((long) page.getObjects().size());
        }
        return page;
    }

    /**
     * Lista objetos paginados procurando por restrições básicas, realizando ordenação.
     * @param parameters parâmetros para filtrar os objetos
     * @param orderBy campo para realizar a ordenação.
     * @param ascending indica se a ordenação é ascendente ou descendente.
     * @param pageNumber número de página desejado
     * @param leftJoins left joins para a query.
     * @return objeto de página contendo a lista de objetos e contagem total dos objetos que correspondem à busca
     */
    public Page<T> paginatedOrderedList(final Map<String, List<String>> parameters, final String orderBy, final boolean ascending, final Short pageNumber,
            final String... leftJoins) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(getEntityType());
        Root<T> from = query.from(getEntityType());
        Predicate predicates = getPredicates(parameters, cb, from, leftJoins);
        if (ascending) {
            query.orderBy(cb.asc(from.get(orderBy)));
        } else {
            query.orderBy(cb.desc(from.get(orderBy)));
        }
        query.where(predicates);
        TypedQuery<T> typedQuery = getEntityManager().createQuery(query);
        // Caso o número de página seja -1, traz todos os resultados.
        if (pageNumber != -1) {
            typedQuery.setMaxResults(RESULTS_PER_PAGE).setFirstResult((pageNumber - 1) * RESULTS_PER_PAGE);
        }
        Page<T> page = new Page<>();
        page.setPageNumber(pageNumber);
        page.setObjects(typedQuery.getResultList());
        // Caso o número de página seja -1, traz todos os resultados.
        if (pageNumber != -1) {
            page.setTotalObjects(count(parameters));
        } else {
            page.setTotalObjects((long) page.getObjects().size());
        }
        return page;
    }

    /**
     * Monta a lista de comparadores da busca a partir do mapa.
     * @param parameters filtros a serem adicionados na busca
     * @param cb criteria builder
     * @param from root
     * @return lista de predicados
     */
    protected Predicate buildPredicate(final Map<String, List<String>> parameters, final CriteriaBuilder cb, final Root<T> from) {
        Predicate predicate = cb.conjunction();
        List<Expression<Boolean>> expressions = predicate.getExpressions();

        for (Map.Entry<String, List<String>> entry : parameters.entrySet()) {
            FilterFactory ff = new FilterFactory().setEntityManager(getEntityManager()).setEntityType(getEntityType()).setCriteriaBuilder(cb);
            expressions.add(ff.setFrom(from).setParameterEntry(entry).prepare().buildFilter());
        }
        return predicate;
    }

    protected Long count(final Map<String, List<String>> parameters, final String... leftJoins) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<T> root = cq.from(getEntityType());

        Predicate restrictions = getPredicates(parameters, cb, root, leftJoins);
        cq.select(cb.countDistinct(root));
        cq.where(restrictions);
        return getEntityManager().createQuery(cq).getSingleResult();
    }

    protected Predicate getPredicates(final Map<String, List<String>> parameters, final CriteriaBuilder cb, final Root<T> from, final String... leftJoins) {
        Map<String, List<String>> parameterMap = parameters;
        if (parameterMap == null) {
            parameterMap = Collections.emptyMap();
        }

        if (leftJoins != null && leftJoins.length > 0) {
            for (String leftJoin : leftJoins) {
                if (!leftJoin.contains(".")) {
                    from.fetch(leftJoin, JoinType.LEFT);
                } else {
                    Join join = null;
                    for (String partialJoin : leftJoin.split("\\.")) {
                        if (join == null) {
                            join = (Join) from.fetch(partialJoin, JoinType.LEFT);
                        } else {
                            join = (Join) join.fetch(partialJoin, JoinType.LEFT);
                        }
                    }
                }
            }
        }
        return buildPredicate(parameterMap, cb, from);
    }
}
