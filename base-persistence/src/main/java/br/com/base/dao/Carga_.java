package br.com.base.dao;

import br.com.base.dao.Carga.Categoria;
import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Carga.class)
public abstract class Carga_ {

	public static volatile SingularAttribute<Carga, Double> volume;
	public static volatile SingularAttribute<Carga, Double> peso;
	public static volatile SingularAttribute<Carga, Categoria> categoria;
	public static volatile SingularAttribute<Carga, Double> valor;
	public static volatile SingularAttribute<Carga, Timestamp> dataChegada;
	public static volatile SingularAttribute<Carga, Long> id;
	public static volatile SingularAttribute<Carga, Timestamp> dataSaida;
	public static volatile SingularAttribute<Carga, String> cepDestino;

}

