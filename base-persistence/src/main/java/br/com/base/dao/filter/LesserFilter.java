package br.com.base.dao.filter;

import java.sql.Timestamp;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Filtro para comparações ce "menor ou igual". Funciona para tipos numéricos ou de data.
 * @author wilerson.oliveira
 */
public class LesserFilter implements Filter {

    private CriteriaBuilder cb;
    private Path path;
    private Object value;

    /**
     * {@inheritDoc}
     */
    @Override
    public Predicate buildPredicate() {
        if (value instanceof Long || value instanceof Double) {
            return cb.le(path, (Number) value);
        }
        return cb.lessThanOrEqualTo(path, (Timestamp) value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LesserFilter setCriteriaBuilder(CriteriaBuilder cb) {
        this.cb = cb;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LesserFilter setPath(Path path) {
        this.path = path;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LesserFilter setConvertedValue(Object value) {
        this.value = value;
        return this;
    }
}
