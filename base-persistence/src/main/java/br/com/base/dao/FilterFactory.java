package br.com.base.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import br.com.base.dao.filter.EmptyFilter;
import br.com.base.dao.filter.EndsFilter;
import br.com.base.dao.filter.EqualsFilter;
import br.com.base.dao.filter.Filter;
import br.com.base.dao.filter.GreaterFilter;
import br.com.base.dao.filter.InJoinFilter;
import br.com.base.dao.filter.InMultipleFilter;
import br.com.base.dao.filter.LesserFilter;
import br.com.base.dao.filter.LikeFilter;
import br.com.base.dao.filter.NotEqualFilter;
import br.com.base.dao.filter.NullFilter;
import br.com.base.dao.filter.StartsFilter;
import br.org.dsd.common.jpa.model.LongModel;

/**
 * Classe para construir predicates a partir de filtros.
 * A ordem usada para as chamadas deve ser usar os setters, depois {@link #prepare()}, depois {@link #buildFilter()}
 * @author wilerson.oliveira
 */
public class FilterFactory {

    private Filter filter;
    private FilterType filterType;
    private String key;
    private String parameterKey;
    private List<String> parameterValue;
    private EntityManager entityManager;
    private Class<? extends LongModel> entityType;
    private Class<?> attributeJavaType;
    private CriteriaBuilder criteriaBuilder;
    private From from;
    private boolean prepared;

    /**
     * Seta o Entity Manager usado para a query
     * @param entityManager o entity manager
     * @return a própria instância, para encadeamento de chamadas
     */
    public FilterFactory setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
        return this;
    }

    /**
     * Seta o tipo de entidade onde será feita a query.
     * @param entityType o tipo da entidade
     * @return a própria instância, para encadeamento de chamadas
     */
    public FilterFactory setEntityType(final Class<? extends LongModel> entityType) {
        this.entityType = entityType;
        return this;
    }

    /**
     * Seta o CriteriaBuilder que será usado para construir a query
     * @param cb o CriteriaBuilder
     * @return a própria instância, para encadeamento de chamadas
     */
    public FilterFactory setCriteriaBuilder(final CriteriaBuilder cb) {
        this.criteriaBuilder = cb;
        return this;
    }

    /**
     * Seta o caminho de origem para a query.
     * @param from o from
     * @return a própria instância, para encadeamento de chamadas
     */
    public FilterFactory setFrom(final From from) {
        this.from = from;
        return this;
    }

    /**
     * Seta a entrada de chave-valor para o parâmetro de busca.
     * Deve ser chamado apenas após {@link #setEntityManager(EntityManager)} e {@link #setEntityType(Class)},
     * para que o tipo do parâmetro seja inferido
     * @param parameterEntry o parâmetro
     * @return a própria instância, para encadeamento de chamadas
     * @throws IllegalStateException caso o Entity Manager ou o tipo não tenham sido setados
     */
    public FilterFactory setParameterEntry(final Map.Entry<String, List<String>> parameterEntry) {
        if (entityManager == null || entityType == null) throw new IllegalStateException();

        parameterKey = parameterEntry.getKey();
        parameterValue = parameterEntry.getValue();
        key = parameterKey;

        for (FilterType type : FilterType.values()) {
            if (type.querystringComponent.length() > 0 && key.endsWith(type.querystringComponent)) {
                filterType = type;
                key = key.replace(type.querystringComponent, "");
            }
        }

        attributeJavaType = getAttributeJavaType(entityManager, entityType, key);
        if (Set.class.equals(attributeJavaType) && !FilterType.IS_EMPTY.equals(filterType)) {
            filterType = FilterType.IN_JOIN;
        }

        if (parameterEntry.getValue().size() > 1) {
            filterType = FilterType.IN_MULTIPLE;
        }

        if (filterType == null) {
            filterType = FilterType.EQUALS;
        }
        return this;
    }

    /**
     * Prepara esta factory para construir o Predicate.
     * Deve ser chamado apenas após {@link #setCriteriaBuilder(CriteriaBuilder)}, {@link #setFrom(From)} e
     * {@link #setParameterEntry(Map.Entry)}, que são as dependências para construir o Predicate
     * @return a própria instância, para encadeamento de chamadas
     * @throws IllegalStateException caso as dependências não tenham sido setadas
     */
    public FilterFactory prepare() {
        if (filterType == null || criteriaBuilder == null || from == null) throw new IllegalStateException();
        try {
            Path path;
            Object value;
            switch(filterType) {
                case IN_MULTIPLE:
                    List<Object> inParameters = new ArrayList<>();
                    for (String valor : parameterValue) {
                        inParameters.add(convertValue(valor, attributeJavaType));
                    }

                    path = getObjectPath(from, key);;
                    value = inParameters;
                    break;

                case IS_NULL:
                    path = getObjectPath(from, key);
                    value = convertValue(parameterValue.get(0), Boolean.class);
                    break;

                case IS_EMPTY:
                    path = getObjectPath(from, key);
                    value = convertValue(parameterValue.get(0), Boolean.class);
                    break;

                case IN_JOIN:
                    path = from.join(key);
                    value = convertValue(parameterValue.get(0), String.class);
                    break;

                default:
                    path = getObjectPath(from, key);
                    value = convertValue(parameterValue.get(0), attributeJavaType);
            }

            filter = filterType.filterClass.newInstance().setCriteriaBuilder(criteriaBuilder).setPath(path).setConvertedValue(value);
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        prepared = true;
        return this;
    }

    /**
     * Efetivamente constrói o predicate com o filtro.
     * Deve ser chamado após {@link #prepare()}.
     * @return o Predicate
     * @throws IllegalStateException caso a instância não tenha sido preparada
     */
    public Predicate buildFilter() {
        if (!prepared) throw new IllegalStateException();
        return filter.buildPredicate();
    }

    /**
     * Método interno para inferir o tipo do parâmetro.
     * @param entityManager o entity manager
     * @param entityType o tipo da classe
     * @param fieldName o nome do campo. Se conter ".", percorre as classes internas
     * @return o tipo do parâmetro
     */
    private Class<?> getAttributeJavaType(final EntityManager entityManager, final Class<? extends LongModel> entityType, final String fieldName) {
        Class type = entityType;
        for (String name : fieldName.split("\\.")) {
            type = entityManager.getMetamodel().entity(type).getAttribute(name).getJavaType();
        }
        return type;
    }

    /**
     * Converte um valor de string para o tipo desejado.
     * @param stringValue valor
     * @param type tipo desejado
     * @return o valor convertido
     */
    private Object convertValue(final String stringValue, final Class<?> type) {
        Object value = stringValue;
        if (Long.class.isAssignableFrom(type) || long.class.isAssignableFrom(type) ||
                BigInteger.class.isAssignableFrom(type) ||
                Integer.class.isAssignableFrom(type) || int.class.isAssignableFrom(type) ||
                Short.class.isAssignableFrom(type) || short.class.isAssignableFrom(type) ||
                Byte.class.isAssignableFrom(type) || byte.class.isAssignableFrom(type)) {

            value = Long.valueOf(stringValue);
        } else if (BigDecimal.class.isAssignableFrom(type) ||
                Double.class.isAssignableFrom(type) || double.class.isAssignableFrom(type) ||
                Float.class.isAssignableFrom(type) || float.class.isAssignableFrom(type)) {
            value = Double.valueOf(stringValue);
        } else if (Boolean.class.isAssignableFrom(type) || boolean.class.isAssignableFrom(type)) {
            value = Boolean.valueOf(stringValue);
        } else if (Timestamp.class.isAssignableFrom(type)) {
            LocalDateTime parsedDate;
            try {
                parsedDate = LocalDateTime.parse(stringValue); // Tenta o ISO format por padrão
            } catch (DateTimeParseException e) {
                try {
                    parsedDate = LocalDateTime.ofInstant(Instant.parse(stringValue), ZoneOffset.UTC);
                } catch (DateTimeParseException e1) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/uuuu");
                    parsedDate = LocalDate.parse(stringValue, formatter).atStartOfDay();
                }
            }
            value = Timestamp.valueOf(parsedDate);
        } else if (type.isEnum()) {
            value = Enum.valueOf((Class<? extends Enum>)type, stringValue);
        }

        return value;
    }

    /**
     * Percorre o caminho da entidade para passar o último nível.
     * @param from caminho inicial
     * @param key chave para o parâmetro
     * @return o caminho final
     */
    private Path getObjectPath(final Path from, final String key) {
        Path path = from;
        for (String rel : key.split("\\.")) {
            path = path.get(rel);
        }
        return path;
    }

    /**
     * Enumeração contendo os tipos de filtro.
     */
    private enum FilterType {
        EQUALS("", EqualsFilter.class),
        LIKE("__like", LikeFilter.class),
        START("__startsWith", StartsFilter.class),
        END("__endsWith", EndsFilter.class),
        IN_JOIN("", InJoinFilter.class),
        IN_MULTIPLE("", InMultipleFilter.class),
        IS_NULL("__isnull", NullFilter.class),
        IS_NOT("__isnot", NotEqualFilter.class),
        GTE("__gte", GreaterFilter.class),
        LTE("__lte", LesserFilter.class),
        IS_EMPTY("__isempty", EmptyFilter.class);

        private String querystringComponent;
        private Class<? extends Filter> filterClass;

        private FilterType(final String querystringComponent, final Class<? extends Filter> filterClass) {
            this.querystringComponent = querystringComponent;
            this.filterClass = filterClass;
        }
    }
}
