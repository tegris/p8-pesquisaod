package br.com.base.dao.filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Filtro para verificar sets vazios.
 * @author wilerson.oliveira
 */
public class EmptyFilter implements Filter {

    private CriteriaBuilder cb;
    private Path path;
    private Boolean condition;

    /**
     * {@inheritDoc}
     */
    @Override
    public Predicate buildPredicate() {
        return condition ? cb.isEmpty(path) : cb.isNotEmpty(path);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EmptyFilter setCriteriaBuilder(final CriteriaBuilder cb) {
        this.cb = cb;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EmptyFilter setPath(final Path path) {
        this.path = path;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EmptyFilter setConvertedValue(final Object value) {
        this.condition = (Boolean) value;
        return this;
    }
}
