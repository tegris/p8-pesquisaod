package br.com.base.dao.filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Filtro para verificar igualdade.
 * @author wilerson.oliveira
 */
public class EqualsFilter implements Filter {

    private CriteriaBuilder cb;
    private Path path;
    private Object value;

    /**
     * {@inheritDoc}
     */
    @Override
    public Predicate buildPredicate() {
        return cb.equal(path, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EqualsFilter setCriteriaBuilder(CriteriaBuilder cb) {
        this.cb = cb;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EqualsFilter setPath(Path path) {
        this.path = path;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EqualsFilter setConvertedValue(Object value) {
        this.value = value;
        return this;
    }
}
