package br.com.base.dao.filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Filtro para conferir se um campo é nulo. O valor passado deve ser um booleano que indique se a busca deve ser "é nulo" ou "não é nulo".
 * @author wilerson.oliveira
 */
public class NullFilter implements Filter {

    private CriteriaBuilder cb;
    private Path path;
    private Boolean condition;

    /**
     * {@inheritDoc}
     */
    @Override
    public Predicate buildPredicate() {
        return condition ? cb.isNull(path) : cb.isNotNull(path);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NullFilter setCriteriaBuilder(CriteriaBuilder cb) {
        this.cb = cb;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NullFilter setPath(Path path) {
        this.path = path;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NullFilter setConvertedValue(Object value) {
        this.condition = (Boolean) value;
        return this;
    }
}
