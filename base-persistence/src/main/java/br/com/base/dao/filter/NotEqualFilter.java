package br.com.base.dao.filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Filtro de not equals.
 * @author gabriel.pinheiro
 */
public class NotEqualFilter implements Filter {

    private CriteriaBuilder cb;
    private Path path;
    private Object value;

    /**
     * {@inheritDoc}
     */
    @Override
    public Predicate buildPredicate() {
        return cb.notEqual(path, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Filter setCriteriaBuilder(final CriteriaBuilder cb) {
        this.cb = cb;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NotEqualFilter setPath(final Path path) {
        this.path = path;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NotEqualFilter setConvertedValue(final Object value) {
        this.value = value;
        return this;
    }

}
