package br.com.base.dao.filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Filtro para comparações de strings terminando com o valor passado.
 */
public class EndsFilter implements Filter {
	
	 private CriteriaBuilder cb;
	 private Path path;
	 private Object value;

	/**
	 * {@inheritDoc} 
	 */
	@Override
	public Predicate buildPredicate() {
        return cb.like(cb.lower(path), "%" + value.toString().toLowerCase());

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Filter setCriteriaBuilder(CriteriaBuilder cb) {
		this.cb = cb;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Filter setPath(Path path) {
		this.path = path;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Filter setConvertedValue(Object value) {
		this.value = value;
		return this;
	}

}
