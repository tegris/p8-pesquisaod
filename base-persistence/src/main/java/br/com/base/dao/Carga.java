package br.com.base.dao;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.base.util.KeyValueInterface;
import br.org.dsd.common.jpa.model.LongModel;

/**
 * Representa uma carga transportada.
 */
@Entity
@Table(name = "carga")
public class Carga extends LongModel {

	private static final long serialVersionUID = -8564447209311962646L;

	@Id
	@SequenceGenerator(allocationSize = 0, name = "carga_seq", sequenceName = "carga_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "carga_seq")
	private Long id;

	@Enumerated(EnumType.STRING)
	private Categoria categoria;

	@Column(name = "peso")
	private double peso;

	@Column(name = "volume")
	private double volume;

	@Column(name = "cep_destino")
	private String cepDestino;

	@Column(name = "data_saida")
	private Timestamp dataSaida;

	@Column(name = "data_chegada")
	private Timestamp dataChegada;

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public String getCepDestino() {
		return cepDestino;
	}

	public void setCepDestino(String cepDestino) {
		this.cepDestino = cepDestino;
	}

	public Timestamp getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Timestamp dataSaida) {
		this.dataSaida = dataSaida;
	}

	public Timestamp getDataChegada() {
		return dataChegada;
	}

	public void setDataChegada(Timestamp dataChegada) {
		this.dataChegada = dataChegada;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	 public String getCategoriaAsString() {
	        return this.categoria.getDescricao();
	    }

	public enum Categoria implements KeyValueInterface {
		CONTAINER("Container"), SECA("Seca"), PERIGOSA("Perigosa"), PERECIVEL(
				"Perecível"), LIQUIDA("Líquida");

		private final String descricao;

		private Categoria(final String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public String getValue() {
			return this.descricao;
		}
	}

}
