package br.com.base.dao.filter;

import java.sql.Timestamp;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Filtro para fazer comparação de "maior ou igual". Funciona para tipos numéricos ou de data.
 * @author wilerson.oliveira
 */
public class GreaterFilter implements Filter {

    private CriteriaBuilder cb;
    private Path path;
    private Object value;

    /**
     * {@inheritDoc}
     */
    @Override
    public Predicate buildPredicate() {
        if (value instanceof Long || value instanceof Double) {
            return cb.ge(path, (Number) value);
        }
        return cb.greaterThanOrEqualTo(path, (Timestamp) value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GreaterFilter setCriteriaBuilder(CriteriaBuilder cb) {
        this.cb = cb;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GreaterFilter setPath(Path path) {
        this.path = path;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GreaterFilter setConvertedValue(Object value) {
        this.value = value;
        return this;
    }
}
