package br.com.base.dao;

/**
 * Classe de acesso ao banco de dados para carga.
 */
public class CargaDAO extends BaseDAO<Carga> {

	@Override
	protected Class<Carga> getEntityType() {
		return Carga.class;
	}

}
