package br.com.base.util;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class SchemaGenerator {

    public static void main(final String[] args) {
        SchemaGenerator.getEM("base-pu");

    }

    /**
     * Entity manager a ser utilizada nos testes.
     * @param nomePersistenceUnit nome da persistence unit
     * @return {@link EntityManager}
     */
    public static EntityManager getEM(final String nomePersistenceUnit) {
        Map<String, String> properties = new HashMap<String, String>();
        configurePostgreSQL(properties);

        EntityManagerFactory emFactory = Persistence.createEntityManagerFactory(nomePersistenceUnit, properties);
        return emFactory.createEntityManager();
    }

    /**
     * Configura as propriedades para utilização do PostgreSQL.
     * @param propriedades mapa utilizado na criação da entity manager
     */
    private static void configurePostgreSQL(final Map<String, String> propriedades) {
        propriedades.put("hibernate.connection.url", "jdbc:postgresql://localhost:5432/base");
        propriedades.put("hibernate.connection.username", "base");
        propriedades.put("hibernate.connection.password", "base");
        propriedades.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        propriedades.put("javax.persistence.jtaDataSource", "base-pu");
        propriedades.put("javax.persistence.transactionType", "RESOURCE_LOCAL");

        propriedades.put("hibernate.hbm2ddl.auto", "update");
        propriedades.put("hibernate.hbm2ddl.import_files", "createDbSchema.sql");


    }
}
