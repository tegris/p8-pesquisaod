package br.com.base.util;

/**
 * Interface para objeto tipo chave-valor.
 */
public interface KeyValueInterface {

    String getKey();

    String getValue();

    default KeyValuePair toKeyValue() {
        return new KeyValuePair(getKey(), getValue());
    }

}