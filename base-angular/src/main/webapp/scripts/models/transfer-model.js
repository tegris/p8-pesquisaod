angular.module('base').service('TransferModel', function(){

	var model = new Object();

	var endpoint = "";
	
	var creationState = "";

	var editionState = "";

	var viewState = "";

	var mode = "";

	var pageTitle = "";
	
	var filters = null;
	
	var showCreate = true;
	
	var showButtons = true;
	
	var showView = true;
	
	var showEdit = true;
	
	var showRemove = true;


	this.setEndpoint = function(e){
		endpoint = e;
	};

	this.getEndpoint = function(){
		return endpoint;
	};


	this.setModel = function(m){
		model = m;
	};

	this.getModel = function(){
		return model;
	};


	this.setMode = function(m){
		mode = m;
	};

	this.getMode = function(){
		return mode;
	};

	this.setCreationState = function(s){
		creationState = s;
	};

	this.getCreationState = function(){
		return creationState;
	};
	

	this.setEditionState = function(s){
		editionState = s;
	};

	this.getEditionState = function(){
		return editionState;
	};


	this.setViewState = function(s){
		viewState = s;
	};

	this.getViewState = function(){
		return viewState;
	};


	this.setPageTitle = function(p){
		pageTitle = p;
	};

	this.getPageTitle = function(){
		return pageTitle;
	};
	
	this.setFilters = function(f) {
		filters = f;
	};
	
	this.getFilters = function() {
		return filters;
	};
	
	this.setShowCreate = function(s) {
		showCreate = s;
	};
	
	this.getShowCreate = function() {
		return showCreate;
	}
	
	this.setShowButtons = function(s) {
		showButtons = s;
	};
	
	this.getShowButtons = function() {
		return showButtons;
	}
	
	this.setShowView = function(s) {
		showView = s;
	};
	
	this.getShowView = function() {
		return showView;
	}
	
	this.setShowEdit = function(s) {
		showEdit = s;
	};
	
	this.getShowEdit = function() {
		return showEdit;
	}
	
	this.setShowRemove = function(s) {
		showRemove = s;
	};
	
	this.getShowRemove = function() {
		return showRemove;
	}
	
	this.clear = function(){
		model = new Object();
		endpoint = "";
		creationState = "";
		editionState = "";
		viewState = "";
		mode = "";
		pageTitle = "";
		filters = null;
		showCreate = true;
		showButtons = true;
	};
	
});