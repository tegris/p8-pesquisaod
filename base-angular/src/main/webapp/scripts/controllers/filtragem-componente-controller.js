angular.module('base').controller('FiltragemController',  function ($rootScope, $scope, FiltragemService) {
	
	/*
		Inicializações.
	*/
	var initController = function(){
		$scope.filtros = [];
	};
	initController();
	
    $scope.$on('filtro-adicionado', function(scope, filtro){
    	$scope.filtros.push(filtro);
    	FiltragemService.adicionarFiltro(filtro);
    	FiltragemService.loadPage();
    });
    
	$scope.removerFiltro = function(filtro){
		$scope.filtros.splice($scope.filtros.indexOf(filtro), 1);
		FiltragemService.removerFiltro(filtro);
		FiltragemService.loadPage();
		$rootScope.$broadcast('filtro-removido', filtro.origem);
	}

});
