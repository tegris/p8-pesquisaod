angular.module('base').controller('PaginacaoController',  function ($scope, PaginacaoService) {

	var reloadPage = function(){
		PaginacaoService.loadPage();
	};
	
	$scope.proximaPagina = function(){
		PaginacaoService.setPaginaAtual(PaginacaoService.getPaginaAtual() + 1);
		reloadPage();
	};
	
	$scope.paginaAnterior = function(){
		PaginacaoService.setPaginaAtual(PaginacaoService.getPaginaAtual() - 1);
		reloadPage();
	};
	
	$scope.irParaPagina = function(pagina){
		if(pagina != PaginacaoService.getPaginaAtual()){
			PaginacaoService.setPaginaAtual(pagina);
			reloadPage();
		}
	};
	
	$scope.paginaAtual = function(){
		return PaginacaoService.getPaginaAtual();
	}
	
	$scope.range = function(){
		var r = [];
		for (i = 1; i <= PaginacaoService.getQuantidadeDePaginas(); i++){
			r.push(i);
		}
		return r;
	}
	
	$scope.totalDePaginas = function(){
		return PaginacaoService.getQuantidadeDePaginas();
	}
	
	
});
