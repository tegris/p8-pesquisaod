angular.module('base').controller('ModelListController',  function (TransferModel, $scope, $rootScope, $state, RestService, Endpoints, PaginacaoService, FiltragemService) {

	var loadModels = function(){
		$scope.mapaFiltragem = new Object();
		$.extend($scope.mapaFiltragem,FiltragemService.getFiltros());
		if(TransferModel.getFilters() != null) {
			$scope.mapaFiltragem = TransferModel.getFilters();
		}
		$scope.permiteCriacao = TransferModel.getShowCreate();
		$scope.exibeBotoes = TransferModel.getShowButtons();
		$scope.mapaFiltragem['pagina'] = PaginacaoService.getPaginaAtual();
		$scope.creationState = TransferModel.getCreationState();
		$scope.showView = TransferModel.getShowView();
		$scope.showEdit = TransferModel.getShowEdit();
		$scope.showRemove = TransferModel.getShowRemove();
		RestService.requests($scope.endpoint, $scope.mapaFiltragem).get().$promise.then(function(data){
			$scope.tableData = data.objects;
			$scope.paginaAtual = data.meta.pagina;
			$scope.camposListagem = data.meta.camposListagem;
			$scope.contagem = data.meta.contagem;
			$scope.showEdit = data.meta.exibeEditar;
			$scope.showView = data.meta.exibeVisualizar;
			$scope.showRemove = data.meta.exibeRemover;
			
			FiltragemService.loadOptions(data.meta.camposListagem);
			PaginacaoService.setQuantidadeDePaginas(Math.ceil(data.meta.contagem/20));
		}, function(promise){
			alertify.error("Erro ao carregar os models.");
		});
	};
	
	var initController = function(){
		$scope.endpoint = TransferModel.getEndpoint();
		$scope.editionState = TransferModel.getEditionState();
		$scope.viewState = TransferModel.getViewState();
		$scope.pageTitle = TransferModel.getPageTitle();
		FiltragemService.init();
		FiltragemService.setModelLoadFunction(loadModels);
		PaginacaoService.setModelLoadFunction(loadModels);
		loadModels();
	};
	initController();
	
});