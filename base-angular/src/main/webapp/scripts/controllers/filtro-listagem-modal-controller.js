angular.module('base').controller('FiltroListagemModalController',  function ($rootScope, $scope, FiltragemService) {

	var initController = function(){
		$scope.textoContem = 1;
		$scope.dataIgual = 1;
		$scope.comparacaoNumerica = 1;
		$scope.campo = "";
		$scope.valor = "";
		$scope.dataSelecionada = "";
		$scope.enumSelecionada = null;
	};
	initController();
	
	var buildBaseFilter = function(){
		var filtro = new Object();
		filtro.exibicao = $scope.campo.exibicao;
		filtro.origem = $scope.campo;
		filtro.campo = $scope.campo.nome;
		return filtro;
	};
	
	var buildFiltroTexto = function(filtro){
		if ($scope.textoContem === 1){
			filtro.exibicao += " contém ";
		} else if($scope.textoContem === 2) {
			filtro.exibicao += " começa com ";
		} else if($scope.textoContem === 3) {
			filtro.exibicao += " termina com ";
		}
		filtro.exibicao += $scope.valor;
		filtro.valor = $scope.valor;
		filtro.textoContem = $scope.textoContem;
		filtro.tipo = "TEXTO";
		return filtro;
	};
	
	var buildFiltroEnum = function(filtro){
		filtro.exibicao += " igual a ";
		filtro.exibicao += $scope.enumSelecionada.value;
		filtro.valor = $scope.enumSelecionada.key;
		filtro.tipo = "ENUM";
		return filtro;
	};
	
	var buildFiltroNumerico = function(filtro){
		if ($scope.comparacaoNumerica === 1){
			filtro.exibicao += " menor que ";
		}else if($scope.comparacaoNumerica === 2){
			filtro.exibicao += " igual a ";
		}else if($scope.comparacaoNumerica === 3){
			filtro.exibicao += " maior que ";
		}else if($scope.comparacaoNumerica === 4){
			filtro.exibicao += " diferente de ";
		}
		filtro.exibicao += $scope.valor;
		filtro.valor = $scope.valor;
		filtro.comparacaoNumerica = $scope.comparacaoNumerica;
		filtro.tipo = "NUMERICO";
		return filtro;
	};
	
	var buildFiltroData = function(filtro){
		if ($scope.dataIgual === 1){
			filtro.exibicao += " anterior a ";
		}else if($scope.dataIgual === 2){
			filtro.exibicao += " igual a ";
		}else if($scope.dataIgual === 3){
			filtro.exibicao += " posterior a ";
		}
		var data = new Date($scope.dataSelecionada);
		filtro.exibicao += data.getDate() + "/" + (data.getMonth()+1) + "/" + data.getFullYear();
		filtro.valor = $scope.dataSelecionada;
		filtro.dataIgual = $scope.dataIgual;
		filtro.tipo = "DATA";
		return filtro;
	};
	
	var resetFiltro = function(){
		FiltragemService.removeOption($scope.campo);
		initController();
	};
	
	var sendFilter = function(filtro){
		resetFiltro();
		$rootScope.$broadcast('filtro-adicionado', filtro);
		$('#filtro-modal').modal('hide');
	};
	$scope.adicionar = function(){
		if($scope.campo){
			var filtro = buildBaseFilter();
			if($scope.isDate()){
				if($scope.dataSelecionada instanceof Date && !isNaN($scope.dataSelecionada.valueOf())){
					filtro = buildFiltroData(filtro);
					sendFilter(filtro);
				}else{
					alertify.error("Informe uma data válida.");
				}
			}else if($scope.isString()){
				if($scope.valor){
					filtro = buildFiltroTexto(filtro);
					sendFilter(filtro);
				}else{
					alertify.error("Informe um valor para a filtragem.");
				}
			}else if($scope.isNumeric()){
				if($scope.valor && !isNaN($scope.valor.valueOf())){
					filtro = buildFiltroNumerico(filtro);
					sendFilter(filtro);
				}else{
					alertify.error("Informe um valor numérico.");
				}
			}else if($scope.isEnum()){
				if($scope.enumSelecionada){
					filtro = buildFiltroEnum(filtro);
					sendFilter(filtro);
				}else{
					alertify.error("Selecione um valor para a " + $scope.campo.exibicao + ".");
				}
			}
		}else{
			alertify.error("Selecione um campo de filtragem.");
		}
	}
	
    $scope.$on('filtro-removido', function(scope, opcao){
    	FiltragemService.addOption(opcao);
    });
    
    $scope.camposDisponiveis = function(){
    	return FiltragemService.getOptions();
    };
    
    $scope.abrirDatePicker = function($event, $log) {
    	$event.preventDefault();
    	$event.stopPropagation();
    	$scope.openedDataSelecionada = true;
    };
    
    $scope.isDate = function(){
    	return $scope.campo && $scope.campo.type === 'Date';
    };
    
    $scope.isString = function(){
    	return $scope.campo && $scope.campo.type === 'String';
    };
    
    $scope.isNumeric = function(){
    	return $scope.campo && $scope.campo.type === 'Integer' || $scope.campo && $scope.campo.type === 'Double';
    };
    
    $scope.isEnum = function(){
    	return $scope.campo && $scope.campo.type.substring(0, 4) == 'Enum';
    };
    
    $scope.opcoesEnum = function(){
    	if($scope.campo.type){
    		var enumDesejada = $scope.campo.type.substring(5, $scope.campo.type.length);
    		return FiltragemService.getEnumOptions(enumDesejada);
    	}
    	return [];
    }
    
});