angular.module('base').directive('componenteFiltragem', function (baseConstants) {
    return {
      restrict: 'E',
      templateUrl: baseConstants.templates.components.filtro
    };
});
