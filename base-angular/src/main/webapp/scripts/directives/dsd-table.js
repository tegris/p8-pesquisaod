angular.module('base').directive('dsdTable', function (baseConstants) {
    return {
      restrict: 'E',
      scope: {keys : '=keys', tableLabels : '=tableLabels', tableData : '=tableData', read: '&', update: '&', remove: '&'},
      templateUrl: baseConstants.templates.components.table,
      link: function(scope, element, attrs){
    	  scope.getField = function(data, path) {
    		  var split = path.split('.');
    		  var result = data;
    		  for (i = 0; i < split.length; i++) {
    			  result = result[split[i]];
    		  }
    		  return result;
    	  };
    	  
    	  scope.evaluate = function(data, path) {
    		  if(typeof path === 'string'){
	    		  var split = path.split('?');
	    		  var exp = scope.getField(data, split[0]);
	    		  var conditional = split[1].split(':');
	    		  var result = '';
	    		  if(conditional[0] === 'check') {
	    			  if(exp) {
	    				  result = conditional[1];
	    			  } else {
	    				  result = conditional[2];
	    			  }
	    		  } else if(conditional[0] === 'equal') {
	    			  result = (exp == conditional[1]);
	    		  } else if(conditional[0] === 'boolean') {
	    			  result =  exp;
	    		  }
	    		  return result;
    		  }
    	  };
      }
  	};
});