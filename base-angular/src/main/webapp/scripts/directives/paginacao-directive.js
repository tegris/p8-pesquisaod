angular.module('base').directive('botoesPaginacao', function (baseConstants) {
    return {
      restrict: 'E',
      templateUrl: baseConstants.templates.paginacao
    };
});
