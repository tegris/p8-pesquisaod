angular.module('base').directive('filtroModal', function (baseConstants) {
    return {
      restrict: 'E',
      templateUrl: baseConstants.templates.modals.filtro
    };
});
