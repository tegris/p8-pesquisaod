/**
 * Serviço genérico para chamadas rest.
 */
angular.module('base').service('RestService', ['$resource',  function($resource){
	this.requests = function(endpoint){
		return this.requests(endpoint, {});
	};
	
	this.requests = function(endpoint, queryParameters){
		return $resource(endpoint,queryParameters,{
			post:{
				method:"POST",
				isArray:false,
			},
			get:{
				method:"GET",
				isArray:false,
			},
			getAll:{
				method:"GET",
				isArray:true,
			},
			update:{
				method:"PUT",
				isArray:false,
			},
	        remove:{
	            method:"DELETE",
	            isArray:false,
	        },
	        postMultipart:{
				method:"POST",
				transformRequest: angular.identity,
				isArray:false,
			}
	    });
	};
}]);