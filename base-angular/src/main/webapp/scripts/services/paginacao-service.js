angular.module('base').service('PaginacaoService', function() {
	
	var paginaAtual = 1;

	var quantidadeDePaginas;
	
	var modelLoadFunction;
	
	this.init = function(){
		paginaAtual = 1;
	};
	
   	this.getPaginaAtual = function(){
  		return paginaAtual;
   	};
   	
   	this.setPaginaAtual = function(p){
   		paginaAtual = p;
   	};
   	
   	this.loadPage = function(){
   		modelLoadFunction();
   	};
   	
   	this.setModelLoadFunction = function(m){
   		modelLoadFunction = m;
   	};
   	
   	this.getQuantidadeDePaginas = function(){
   		return quantidadeDePaginas;
   	};
   	
   	this.setQuantidadeDePaginas = function(q){
   		quantidadeDePaginas = q;
   	};
   	
   	
});