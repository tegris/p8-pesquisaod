angular.module('base').service('FiltragemService', function(RestService, Endpoints) {
	
	var filtros;

	var options;
	
	var enumOptions;
	
	var modelLoadFunction;
	
	var loadEnumOptions = function(e){
		RestService.requests(Endpoints.Base.enums.replace(':enum_desejada', e)).get().$promise.then(function(opcoes){
			enumOptions[e] = opcoes.objects[0];
		}, function(promise){
			alertify.error("Não foi possível carregar as opções de enum.");
			console.log(promise);
		});
	}
	
	this.init = function(){
		filtros = new Object();
		enumOptions = new Object();
		options = null;
		modelLoadFunction = null;
	};
	
   	this.getFiltros = function(){
  		return filtros;
   	};
   	
   	this.adicionarFiltro = function(filtro){
		var chave = filtro.campo; 
		if (filtro.tipo==="TEXTO"){
			if (filtro.textoContem === 1){
				chave += '__like';
			} else if(filtro.textoContem === 2) {
				chave += '__startsWith'
			} else if(filtro.textoContem === 3) {
				chave += '__endsWith'
			}
			filtros[chave] = filtro.valor;
		}else if (filtro.tipo==="DATA"){
			if (filtro.dataIgual === 1){ //Data anterior à data selecionada.
				chave += '__lte';
				filtros[chave] = new Date(filtro.valor.setDate(filtro.valor.getDate()-1));
			}else if(filtro.dataIgual === 2){ //Data igual à data selecionada.
				var f = $.extend({}, filtro);
				f.valor = new Date(f.valor);
				f.valor.setUTCHours(23,59,59,999);
				var c = filtro.campo + '__lte';
				filtros[c] = f.valor;
				
				chave += '__gte';
				filtro.valor.setUTCHours(00,00,00,000);
				filtros[chave] = new Date(filtro.valor);
			}else if(filtro.dataIgual === 3){ //Data posterior à data selecionada.
				chave += '__gte';
				filtros[chave] = new Date(filtro.valor.setDate(filtro.valor.getDate()));
			}
		}else if (filtro.tipo==="NUMERICO"){
			
			if (filtro.comparacaoNumerica === 1){ //Números menores do que o valor informado.
				chave += '__lte';
				filtros[chave] = parseFloat(filtro.valor) - 0.01;
			}else if(filtro.comparacaoNumerica === 2){ //Números iguais ao valor informado.
				chave += '__gte';
				var f = $.extend({}, filtro);
				var c = filtro.campo + '__lte';
				filtros[c] = f.valor;
				filtros[chave] = filtro.valor;
			}else if(filtro.comparacaoNumerica === 3){ //Números maiores do que o valor informado.
				chave += '__gte';
				filtros[chave] = parseFloat(filtro.valor) + 0.01;
			}else if(filtro.comparacaoNumerica === 4){ //Números diferentes do valor informado.
				chave += '__isnot';
				filtros[chave] = filtro.valor;
			} 
			
		}else if(filtro.tipo === "ENUM"){
			chave = chave.replace('AsString', '');
			filtros[chave] = filtro.valor;
		}
   	};
   	
   	this.removerFiltro = function(filtro){
		var remocao = filtro.campo;
		if (filtro.tipo==="TEXTO"){
			if (filtro.textoContem === 1){
				remocao += '__like';
			} else if(filtro.textoContem === 2) {
				remocao += '__startsWith'
			} else if(filtro.textoContem === 3) {
				remocao += '__endsWith'
			}
		}else if (filtro.tipo==="DATA"){
			if (filtro.dataIgual === 1){ //Data anterior à data selecionada.
				remocao += '__lte';
			}else if(filtro.dataIgual === 2){ //Data igual à data selecionada.
				remocao += '__lte';
				var r = filtro.campo + '__gte';
				delete filtros[r];
			}else if(filtro.dataIgual === 3){ //Data posterior à data selecionada.
				remocao += '__gte';
			}
		}else if (filtro.tipo==="NUMERICO"){
			if (filtro.comparacaoNumerica === 1){ //Números menores do que o valor informado.
				remocao += '__lte';
			}else if(filtro.comparacaoNumerica === 2){ //Números iguais ao valor informado.
				remocao += '__lte';
				var r = filtro.campo + '__gte';
				delete filtros[r];
			}else if(filtro.comparacaoNumerica === 3){ //Números maiores do que o valor informado.
				remocao += '__gte';
			}else if(filtro.comparacaoNumerica === 4){ //Números diferentes do valor informado.
				remocao += '__isnot';
			} 
		}else if(filtro.tipo === "ENUM"){
			remocao = remocao.replace('AsString', '');
		}
		delete filtros[remocao];
   	};
   	
   	this.loadOptions = function(tOptions){
   		if (options === null){
   			options = tOptions;
   			var i = 0;
   			for(i=0;i<options.length;i++){
   				if(options[i].type.substring(0, 4) == 'Enum'){
   					var o = options[i].type.substring(5, options[i].type.length);
   					enumOptions[o] = [];
   					loadEnumOptions(o);
   				}
   			}
   			i=0;
   			while(i<options.length && options[i].nome != 'centroCusto'){
   				i++;
   			}
   			options.splice(i, 1);
   		}
   	};
   	
   	this.addOption = function(o){
   		options.push(o);
   	};
   	
   	this.removeOption = function(o){
   		var i = 0;
   		while(i<options.length && options[i].nome != o.nome){
   			i++;
   		}
   		options.splice(i, 1);
   	};
   	
   	this.getEnumOptions = function(e){
		return enumOptions[e]; 
   	};
   	
   	this.getOptions = function(){
   		return options;
   	};
   	
   	this.loadPage = function(){
   		modelLoadFunction();
   	};
   	
   	this.setModelLoadFunction = function(m){
   		modelLoadFunction = m;
   	};
});