var baseApp = angular.module('base',
  ['base.endpoints',
   'ui.router',
   'ui.bootstrap',
   'ngResource'
  ]).constant('baseConstants',{
		templates: {
			list: 'views/components/dsd/model-list.html',
			paginacao: 'views/partials/base/paginacao.html',			
			components: {
				table: 'views/components/dsd/dsd-table.html',				
				filtro: 'views/components/dsd/componente-filtragem.html',
				data: 'views/components/dsd/componente-data.html',
				erros: 'views/components/dsd/componente-erros.html'
			},			
			modals: {
				filtro: 'views/modals/dsd/filtro-modal.html',
			},
		},
		urls: {
			base: "/base",
			carga: "views/components/dsd/model-list.html"
		}
	}).config(function($stateProvider, $urlRouterProvider, baseConstants) {

		// States
		$stateProvider
		.state('base', {
			name: 'base',
			abstract: true,
			url: baseConstants.urls.base,
			template: "<div ui-view></div>",
			data: {
		        requireLogin: true
			}
		})
		.state('base.init', { 
			name: 'base.init',
			url: baseConstants.urls.login,
			templateUrl: baseConstants.templates.login, 
			requireLogin: false
		})
		.state('base.consultaCarga', { 
			name: 'base.consultaCarga',
			url: baseConstants.urls.carga,
			templateUrl: baseConstants.urls.carga, 
			requireLogin: false
		});
	});

baseApp.run(function ($rootScope, $state, TransferModel, Endpoints) {
//	TransferModel.setCreationState("base.cadastroCarga");
//	TransferModel.setEditionState("base.editarCarga");
//	TransferModel.setViewState("base.visualizarCarga");
	TransferModel.setEndpoint(Endpoints.Base.carga.listar);
	TransferModel.setPageTitle("Cargas");
    $state.go("base.consultaCarga", {}, {"reload": true});
    
	$rootScope.is = function(name) {
		return $state.is(name);
	}
	$rootScope.includes = function(name) {
		return $state.includes(name);
	}
	
});