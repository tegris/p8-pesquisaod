package br.com.base.meta;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import br.com.base.dao.Page;

/**
 * Container para as respostas dos endpoints.
 * @param <T> tipo dos objetos contidos
 * @author wilerson.oliveira
 */
public class Container<T> implements Serializable {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -1620979151559697345L;

    /**
     * Objetos contidos na resposta.
     */
    private Collection<T> objects;

    /**
     * Metadados para a resposta.
     */
    private Meta meta;

    /**
     * Construtor default.
     */
    public Container() {

    }

    /**
     * Construtor com todos os parâmetros.
     * @param camposListagem campos da listagem dos metadados
     * @param pagina página dos metadados
     * @param contagem contagem de registro dos metadados
     * @param objects lista de objetos
     */
    public Container(final List<MetaField> camposListagem, final Short pagina, final Long contagem, final Collection<T> objects) {
        this.meta = new Meta(camposListagem, pagina, contagem);
        this.objects = objects;
    }

    /**
     * Construtor para um objeto único.
     * @param camposListagem campos da listagem dos metadados
     * @param object objeto único
     */
    public Container(final List<MetaField> camposListagem, final T object) {
        this.meta = new Meta(camposListagem, (short) 1, 1L);
        this.addObject(object);
    }

    /**
     * Construtor que obtém os parâmetros a partir do objeto de página do banco de dados.
     * @param camposListagem campos da listagem dos metadados
     * @param page objeto de página do banco de dados
     */
    public Container(final List<MetaField> camposListagem, final Page<T> page) {
        this.meta = new Meta(camposListagem, page.getPageNumber(), page.getTotalObjects());
        this.objects = page.getObjects();
    }

    public Container(final List<MetaField> camposListagem, final Page<T> page, final MetaButton metaButton) {
        this.meta = new Meta(camposListagem, page.getPageNumber(), page.getTotalObjects());
        this.meta.setMetaButtons(metaButton);
        this.objects = page.getObjects();
    }

    /**
     * Adiciona um objeto à lista.
     * @param object o objeto
     */
    @SafeVarargs
    public final void addObject(final T... object) {
        if (objects == null) {
            objects = new ArrayList<>();
        }
        objects.addAll(Arrays.asList(object));
    }

    /**
     * Retorna os objetos para a resposta.
     * @return os objetos
     */
    public Collection<T> getObjects() {
        return objects;
    }

    /**
     * Informa os objetos para a resposta.
     * @param objects os objetos
     */
    public void setObjects(final Collection<T> objects) {
        this.objects = objects;
    }

    /**
     * Retorna os metadados da resposta.
     * @return os metadados
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * Informa os metadados para a resposta.
     * @param meta os metadados
     */
    public void setMeta(final Meta meta) {
        this.meta = meta;
    }
}
