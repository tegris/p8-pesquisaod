package br.com.base.meta;

import java.io.Serializable;

/**
 * Metadados para campos nas listagens dos endpoints.
 * @author wilerson.oliveira
 */
public class MetaField implements Serializable {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -5246044263943004101L;

    /**
     * Nome do campo.
     */
    private String nome;

    /**
     * Descrição para exibir o nome do campo na listagem.
     */
    private String exibicao;

    /**
     * Tipo do campo para exibição.
     */
    private String type;

    /**
     * Construtor padrão.
     */
    public MetaField() {

    }

    /**
     * Construtor com todos os argumentos
     * @param nome nome do campo
     * @param exibicao descrição
     * @param type tipo
     */
    public MetaField(final String nome, final String exibicao, final String type) {
        this.nome = nome;
        this.exibicao = exibicao;
        this.type = type;
    }

    /**
     * Retorna o nome do campo.
     * @return o nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Informa o nome do campo.
     * @param nome o nome
     */
    public void setNome(final String nome) {
        this.nome = nome;
    }

    /**
     * Retorna a descrição do campo para exibição.
     * @return a descrição
     */
    public String getExibicao() {
        return exibicao;
    }

    /**
     * Informa a descrição do campo.
     * @param exibicao a descrição
     */
    public void setExibicao(final String exibicao) {
        this.exibicao = exibicao;
    }

    /**
     * Retorna o tipo do campo.
     * @return o tipo
     */
    public String getType() {
        return type;
    }

    /**
     * Informa o tipo do campo.
     * @param type o tipo
     */
    public void setType(final String type) {
        this.type = type;
    }
}
