package br.com.base.meta;

import java.io.Serializable;

/**
 * Metadados para botões exibidos na tela de consulta.
 */
public class MetaButton implements Serializable {

    private String exibeEditar = "true";

    private String exibeVisualizar = "true";

    private String exibeRemover = "true";

    public MetaButton() {

    }

    public MetaButton(final String exibeEditar, final String exibeVisualizar, final String exibeRemover) {
        this.exibeEditar = exibeEditar;
        this.exibeVisualizar = exibeVisualizar;
        this.exibeRemover = exibeRemover;
    }

    public String getExibeEditar() {
        return exibeEditar;
    }

    public void setExibeEditar(final String exibeEditar) {
        this.exibeEditar = exibeEditar;
    }

    public String getExibeVisualizar() {
        return exibeVisualizar;
    }

    public void setExibeVisualizar(final String exibeVisualizar) {
        this.exibeVisualizar = exibeVisualizar;
    }

    public String getExibeRemover() {
        return exibeRemover;
    }

    public void setExibeRemover(final String exibeRemover) {
        this.exibeRemover = exibeRemover;
    }

}
