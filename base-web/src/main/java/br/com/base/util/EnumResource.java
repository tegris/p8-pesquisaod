package br.com.base.util;

import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.base.dao.Page;
import br.com.base.meta.Container;

import com.google.common.collect.Lists;

/**
 * Endpoint referente a enums usadas pelo filtro.
 */
@Path("enum")
public class EnumResource {

    @GET
    @Path("{enumDesejada}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEnum(@PathParam("enumDesejada") final Enums enumDesejada) {
        Page<Object> page = new Page<Object>();
        page.setObjects(enumDesejada.getValues());
        Container<Object> container = new Container<Object>(Lists.newArrayList(), page);
        return Response.ok().entity(container).build();
    }

    public enum Enums {
        CATEGORIA_CARGA(br.com.base.dao.Carga.Categoria.class);

    	
        private Class<? extends Enum> enumCorrespondente;

        private Enums(final Class<? extends Enum> enumCorrespondente) {
            this.enumCorrespondente = enumCorrespondente;
        }

        @SuppressWarnings("unchecked")
        public Set getValues() {
            return (Set) EnumSet.allOf(enumCorrespondente).stream().map(e -> ((KeyValueInterface) e).toKeyValue()).collect(Collectors.toSet());
        }
    }

}