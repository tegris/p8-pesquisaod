package br.com.base.exception.mapper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import br.org.dsd.common.jpa.repository.RegistroNaoEncontradoException;

/**
 * Mapeamento das exceções de registro não encontrado.
 * @author raphael.pinheiro
 */
@Provider
public class RegistroNaoEncontradoExceptionMapper implements ExceptionMapper<RegistroNaoEncontradoException> {
    /**
     * Logger do mapper.
     */
    private static final Logger LOG = Logger.getLogger(RegistroNaoEncontradoExceptionMapper.class);

    @Override
    public Response toResponse(final RegistroNaoEncontradoException exception) {
        LOG.error(exception.getMessage(), exception);
        Map<String, List<String>> erros = new HashMap<>();
        erros.put("erros", Collections.singletonList(exception.getMessage()));
        return Response.status(Response.Status.NOT_FOUND).entity(erros).type(MediaType.APPLICATION_JSON).build();
    }

}
