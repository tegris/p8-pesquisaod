package br.com.base;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.ser.BeanPropertyWriter;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.hibernate.Hibernate;
import org.hibernate.collection.spi.PersistentCollection;

import br.org.dsd.common.jpa.model.LongModel;

/**
 * Filtro para evitar serialização de coleções não inicializadas pelo JPA.
 */
public class LazyFilter extends SimpleBeanPropertyFilter {

    public static final String NAME = "lazyFilter";

    public LazyFilter() {
    }

    public void serializeAsField(Object bean, JsonGenerator jgen, SerializerProvider prov, BeanPropertyWriter writer) throws Exception {
        if (writer.get(bean) != null && (!this.entityBeanOrPersistentCollection(bean, writer) || Hibernate.isInitialized(writer.get(bean)))) {
            writer.serializeAsField(bean, jgen, prov);
        }
    }

    private boolean entityBeanOrPersistentCollection(Object bean, BeanPropertyWriter writer) throws Exception {
        return LongModel.class.isAssignableFrom(writer.getType().getRawClass()) || PersistentCollection.class.isAssignableFrom(writer.get(bean).getClass());
    }
}
