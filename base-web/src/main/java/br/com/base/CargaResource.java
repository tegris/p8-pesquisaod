package br.com.base;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

import br.com.base.business.CargaService;
import br.com.base.dao.Carga;
import br.com.base.dao.Page;
import br.com.base.meta.MetaField;

/**
 * Endpoint referente a listagem de cargas.
 */
@Path("/carga")
@Stateless
public class CargaResource extends BaseResource<Carga> {
	
	@Inject
	CargaService service;
	
	@Override
    protected Page<Carga> obtainResources(Map<String, List<String>> queryParameters, Short pageNumber) {
        return service.paginatedList(queryParameters, pageNumber);
    }

	@Override
	protected List<MetaField> generateCamposListagem() {
		return Arrays.asList(new MetaField("categoriaAsString", "Categoria", "Enum:CATEGORIA_CARGA"), new MetaField("peso", "Peso", "Double"), new MetaField("volume", "Volume", "Double"), new MetaField("cepDestino", "CEP Destino", "String"), new MetaField("dataSaida", "Data Saída", "Date"), new MetaField("dataChegada", "Data Chegada", "Date"));
	}

}
